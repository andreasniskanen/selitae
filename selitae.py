#!/usr/bin/env python3

"""
Copyright (c) <2020-2022> <Andreas Niskanen, University of Helsinki>



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:



The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import os, sys, itertools, subprocess, resource

path_to_forqes = ""
path_to_maxsat = ""
path_to_qbfsmuser = ""
maxsat_solver = ""

def check_int(s):
    if len(s) == 0:
        return False
    if s[0] in ['-','+']:
        return s[1:].isdigit()
    return s.isdigit()

class AF():

    def __init__(self, args, atts, filename="", query=None, semantics=None, att_mode=None, skept_mode=None):
        self.filename = filename

        self.args = list(range(len(args)))
        self.int_to_arg = args
        self.arg_to_int = { self.int_to_arg[arg] : arg for arg in self.args }

        self.atts = [(self.arg_to_int[s], self.arg_to_int[t]) for s,t in atts]
        self.attackers = { a : [] for a in self.args }
        for a,b in self.atts:
            self.attackers[b].append(a)

        if not (query is None):
            self.query = self.arg_to_int[query]
            self.semantics = semantics
            self.att_mode = att_mode
            self.skept_mode = skept_mode
            
            self.var_counter = itertools.count(1)
            self.arg_accepted_var = { a : next(self.var_counter) for a in self.args }
            if self.semantics != "stb":
                self.arg_rejected_var = { a : next(self.var_counter) for a in self.args }

            if not self.att_mode:
                self.arg_exists_var = { a : next(self.var_counter) for a in self.args }
                if self.semantics == "com":
                    self.arg_exists_not_rejected_var = { a : next(self.var_counter) for a in self.args }
            else:
                self.att_exists_var = { (a,b) : next(self.var_counter) for a,b in self.atts }
                self.source_accepted_var = { (a,b) : next(self.var_counter) for a,b in self.atts }
                if self.semantics == "com":
                    self.source_not_rejected_var = { (a,b) : next(self.var_counter) for a,b in self.atts }

            self.number_of_vars = next(self.var_counter)-1

def source_accepted_clauses(af):
    clauses = []
    for a,b in af.atts:
        clauses.append([-af.source_accepted_var[(a,b)], af.att_exists_var[(a,b)]])
        clauses.append([-af.source_accepted_var[(a,b)], af.arg_accepted_var[a]])
        clauses.append([af.source_accepted_var[(a,b)], -af.att_exists_var[(a,b)], -af.arg_accepted_var[a]])
    return clauses

def arg_rejected_clauses(af):
    clauses = []
    for a in af.args:
        clauses.append([-af.arg_rejected_var[a]] + [af.arg_accepted_var[b] for b in af.attackers[a]])
        for b in af.attackers[a]:
            clauses.append([af.arg_rejected_var[a], -af.arg_accepted_var[b]])
    return clauses

def att_rejected_clauses(af):
    clauses = source_accepted_clauses(af)
    for a in af.args:
        clauses.append([-af.arg_rejected_var[a]] + [af.source_accepted_var[(b,a)] for b in af.attackers[a]])
        for b in af.attackers[a]:
            clauses.append([af.arg_rejected_var[a], -af.source_accepted_var[(b,a)]])
    return clauses

def arg_exists_not_rejected_clauses(af):
    clauses = []
    for a in af.args:
        clauses.append([-af.arg_exists_not_rejected_var[a], af.arg_exists_var[a]])
        clauses.append([-af.arg_exists_not_rejected_var[a], -af.arg_rejected_var[a]])
        clauses.append([af.arg_exists_not_rejected_var[a], -af.arg_exists_var[a], af.arg_rejected_var[a]])
    return clauses

def source_not_rejected_clauses(af):
    clauses = []
    for a,b in af.atts:
        clauses.append([-af.source_not_rejected_var[(a,b)], af.att_exists_var[(a,b)]])
        clauses.append([-af.source_not_rejected_var[(a,b)], -af.arg_rejected_var[a]])
        clauses.append([af.source_not_rejected_var[(a,b)], -af.att_exists_var[(a,b)], af.arg_rejected_var[a]])
    return clauses

def cf_arg_clauses(af):
    clauses = [[af.arg_exists_var[a], -af.arg_accepted_var[a]] for a in af.args]
    for a in af.args:
        for b in af.attackers[a]:
            clauses.append([-af.arg_accepted_var[a], -af.arg_accepted_var[b]])
    return clauses

def cf_att_clauses(af):
    clauses = []
    for a in af.args:
        for b in af.attackers[a]:
            clauses.append([-af.att_exists_var[(b,a)], -af.arg_accepted_var[a], -af.arg_accepted_var[b]])
    return clauses

def adm_arg_clauses(af):
    clauses = cf_arg_clauses(af)
    clauses.extend(arg_rejected_clauses(af))
    for a in af.args:
        for b in af.attackers[a]:
            clauses.append([-af.arg_exists_var[b], -af.arg_accepted_var[a], af.arg_rejected_var[b]])
    if af.skept_mode and af.semantics == "adm":
        clauses.append([af.arg_accepted_var[a] for a in af.args])
    return clauses

def adm_att_clauses(af):
    clauses = cf_att_clauses(af)
    clauses.extend(att_rejected_clauses(af))
    for a in af.args:
        for b in af.attackers[a]:
            clauses.append([-af.att_exists_var[(b,a)], -af.arg_accepted_var[a], af.arg_rejected_var[b]])
    if af.skept_mode and af.semantics == "adm":
        clauses.append([af.arg_accepted_var[a] for a in af.args])
    return clauses

def com_arg_clauses(af):
    clauses = adm_arg_clauses(af)
    clauses.extend(arg_exists_not_rejected_clauses(af))
    for a in af.args:
        clauses.append([-af.arg_exists_var[a]] + [af.arg_exists_not_rejected_var[b] for b in af.attackers[a]] + [af.arg_accepted_var[a]])
    return clauses

def com_att_clauses(af):
    clauses = adm_att_clauses(af)
    clauses.extend(source_not_rejected_clauses(af))
    for a in af.args:
        clauses.append([af.source_not_rejected_var[(b,a)] for b in af.attackers[a]] + [af.arg_accepted_var[a]])
    return clauses

def stb_arg_clauses(af):
    clauses = cf_arg_clauses(af)
    for a in af.args:
        clauses.append([-af.arg_exists_var[a], af.arg_accepted_var[a]] + [af.arg_accepted_var[b] for b in af.attackers[a]])
    return clauses

def stb_att_clauses(af):
    clauses = cf_att_clauses(af)
    clauses.extend(source_accepted_clauses(af))
    for a in af.args:
        clauses.append([af.arg_accepted_var[a]] + [af.source_accepted_var[(b,a)] for b in af.attackers[a]])
    return clauses

def write_cnf_to_file(filename, number_of_vars, clauses):
    output = open(filename, "w")
    output.write("p cnf " + str(number_of_vars) + " " + str(len(clauses)) + "\n")
    for clause in clauses:
        output.write(" ".join(map(str, clause)) + " 0\n")
    output.close()

def write_wcnf_to_file(filename, number_of_vars, hard_clauses, soft_clauses):
    top = len(soft_clauses) + 1
    output = open(filename, "w")
    output.write("p wcnf " + str(number_of_vars) + " " + str(len(hard_clauses) + len(soft_clauses)) + " " + str(top) + "\n")
    for clause in hard_clauses:
        output.write(str(top) + " " + " ".join(map(str, clause)) + " 0\n")
    for clause in soft_clauses:
        output.write(str(1) + " " + " ".join(map(str, clause)) + " 0\n")
    output.close()

def write_qcnf_to_file(af, filename, number_of_vars, universal_vars, existential_vars, clauses):
    output = open(filename, "w")
    if not af.att_mode:
        for a in af.args:
            output.write("c " + str(af.arg_exists_var[a]) + " " + af.int_to_arg[a] + "\n")
    else:
        for a,b in af.atts:
            output.write("c " + str(af.att_exists_var[(a,b)]) + " (" + af.int_to_arg[a] + "," + af.int_to_arg[b] + ")\n")
    output.write("p cnf " + str(number_of_vars) + " " + str(len(clauses)) + "\n")
    output.write("a " + " ".join(list(map(str, universal_vars))) + " 0\n")
    output.write("e " + " ".join(list(map(str, existential_vars))) + " 0\n")
    for clause in clauses:
        output.write(" ".join(map(str, clause)) + " 0\n")
    output.close()

def smallest_explanation(af):
    if af.skept_mode:
        base_clauses = [[-af.arg_accepted_var[af.query]]]
    else:
        base_clauses = [[af.arg_accepted_var[af.query]]]
    mus_clauses = []
    if not af.att_mode:
        if af.semantics == "adm":
            base_clauses.extend(adm_arg_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_arg_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_arg_clauses(af))
        mus_clauses = [[af.arg_exists_var[a]] for a in af.args]
    else:
        if af.semantics == "adm":
            base_clauses.extend(adm_att_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_att_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_att_clauses(af))
        mus_clauses = [[af.att_exists_var[(a,b)]] for a,b in af.atts]
    write_cnf_to_file(af.filename + ".cnf", af.number_of_vars, base_clauses)
    write_cnf_to_file(af.filename + ".mus", af.number_of_vars, mus_clauses)
    start_time = resource.getrusage(resource.RUSAGE_CHILDREN)
    try:
        output = subprocess.check_output([path_to_forqes, "-vv", "-n", af.filename + ".cnf", af.filename + ".mus"]).decode("utf-8").split("\n")
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf-8").split("\n")
    end_time = resource.getrusage(resource.RUSAGE_CHILDREN)
    cpu_time = end_time.ru_utime - start_time.ru_utime
    opt = next((line for line in reversed(output) if line.startswith("o ")), "SAT")
    mus = map(int, filter(lambda x: (check_int(x) and x != "0"), next((line for line in output if line.startswith("v ")), "").split(" ")))
    print(opt)
    print("c Forqes total time:", round(cpu_time,2))
    if opt == "SAT":
        return
    if not af.att_mode:
        args = [af.args[i] for i in map(lambda x: x-1, mus)]
        print("c arguments:", " ".join([af.int_to_arg[a] for a in args]))
    else:
        atts = [af.atts[i] for i in map(lambda x: x-1, mus)]
        print("c attacks:", " ".join(["(" + af.int_to_arg[s] + "," + af.int_to_arg[t] + ")" for s,t in atts]))
    os.remove(af.filename + ".cnf")
    os.remove(af.filename + ".mus")

def smallest_diagnosis(af):
    if af.skept_mode:
        base_clauses = [[-af.arg_accepted_var[af.query]]]
    else:
        base_clauses = [[af.arg_accepted_var[af.query]]]
    soft_clauses = []
    if not af.att_mode:
        if af.semantics == "adm":
            base_clauses.extend(adm_arg_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_arg_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_arg_clauses(af))
        soft_clauses = [[af.arg_exists_var[a]] for a in af.args]
    else:
        if af.semantics == "adm":
            base_clauses.extend(adm_att_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_att_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_att_clauses(af))
        soft_clauses = [[af.att_exists_var[(a,b)]] for a,b in af.atts]
    write_wcnf_to_file(af.filename + ".wcnf", af.number_of_vars, base_clauses, soft_clauses)
    if maxsat_solver == "rc2":
        start_time = resource.getrusage(resource.RUSAGE_CHILDREN)
        output = subprocess.check_output([path_to_maxsat, "-vv", af.filename + ".wcnf"]).decode("utf-8").split("\n")
        end_time = resource.getrusage(resource.RUSAGE_CHILDREN)
        cpu_time = end_time.ru_utime - start_time.ru_utime
    else:
        start_time = resource.getrusage(resource.RUSAGE_CHILDREN)
        try:
            output = subprocess.check_output([path_to_maxsat, af.filename + ".wcnf"]).decode("utf-8").split("\n")
        except subprocess.CalledProcessError as e:
            output = e.output.decode("utf-8").split("\n")
        end_time = resource.getrusage(resource.RUSAGE_CHILDREN)
        cpu_time = end_time.ru_utime - start_time.ru_utime
    opt = next((line for line in reversed(output) if line.startswith("o ")), "UNSAT")
    assignment = map(int, filter(lambda x: check_int(x), next((line for line in output if line.startswith("v ")), "").split(" ")))
    print(opt)
    print("c MaxSAT total time:", round(cpu_time,2))
    if opt == "UNSAT":
        return
    assignment = list(assignment)
    if not af.att_mode:
        args = [a for a in af.args if assignment[af.arg_exists_var[a]-1] < 0]
        print("c arguments:", " ".join([af.int_to_arg[a] for a in args]))
    else:
        atts = [(a,b) for a,b in af.atts if assignment[af.att_exists_var[a,b]-1] < 0]
        print("c attacks:", " ".join(["(" + af.int_to_arg[s] + "," + af.int_to_arg[t] + ")" for s,t in atts]))
    os.remove(af.filename + ".wcnf")

def smallest_second_level(af, dual=False):
    if dual: # need to change the meaning of "arg/att exists" to its negation
        if not af.att_mode:
            af.arg_exists_var = { a : -af.arg_exists_var[a] for a in af.args }
        else:
            af.att_exists_var = { (a,b) : -af.att_exists_var[(a,b)] for a,b in af.atts }
    if af.skept_mode:
        base_clauses = [[-af.arg_accepted_var[af.query]]]
    else:
        base_clauses = [[af.arg_accepted_var[af.query]]]
    existential_vars = [ af.arg_accepted_var[a] for a in af.args ]
    if af.semantics != "stb":
        existential_vars += [ af.arg_rejected_var[a] for a in af.args ]
    if not af.att_mode:
        if af.semantics == "adm":
            base_clauses.extend(adm_arg_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_arg_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_arg_clauses(af))
        universal_vars = [ abs(af.arg_exists_var[a]) for a in af.args]
        if af.semantics == "com":
            existential_vars += [ af.arg_exists_not_rejected_var[a] for a in af.args ]
    else:
        if af.semantics == "adm":
            base_clauses.extend(adm_att_clauses(af))
        elif af.semantics == "com":
            base_clauses.extend(com_att_clauses(af))
        elif af.semantics == "stb":
            base_clauses.extend(stb_att_clauses(af))
        universal_vars = [ abs(af.att_exists_var[(a,b)]) for a,b in af.atts]
        existential_vars += [ af.source_accepted_var[(a,b)] for a,b in af.atts ]
        if af.semantics == "com":
            existential_vars += [ af.source_not_rejected_var[(a,b)] for a,b in af.atts ]
    write_qcnf_to_file(af, af.filename + ".qcnf", af.number_of_vars, universal_vars, existential_vars, base_clauses)
    start_time = resource.getrusage(resource.RUSAGE_CHILDREN)
    try:
        if not dual:
            output = subprocess.check_output([path_to_qbfsmuser, af.filename + ".qcnf"]).decode("utf-8").split("\n")
        else:
            output = subprocess.check_output([path_to_qbfsmuser, "--dual", af.filename + ".qcnf"]).decode("utf-8").split("\n")
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf-8").split("\n")
    end_time = resource.getrusage(resource.RUSAGE_CHILDREN)
    cpu_time = end_time.ru_utime - start_time.ru_utime
    opt = next((line for line in reversed(output) if line.startswith("o ")), "SAT")
    mus = map(int, filter(lambda x: (check_int(x) and x != "0"), next((line for line in output if line.startswith("v ")), "").split(" ")))
    print(opt)
    print("c QBF-SMUSer total time:", round(cpu_time,2))
    if opt == "SAT":
        return
    if af.semantics == "stb":
        offset = len(af.args)
    else:
        offset = 2*len(af.args)
    if not af.att_mode:
        args = [af.args[i] for i in map(lambda x: x-offset-1, mus)]
        print("c arguments:", " ".join([af.int_to_arg[a] for a in args]))
    else:
        atts = [af.atts[i] for i in map(lambda x: x-offset-1, mus)]
        print("c attacks:", " ".join(["(" + af.int_to_arg[s] + "," + af.int_to_arg[t] + ")" for s,t in atts]))
    os.remove(af.filename + ".qcnf")

def parse_af(filename):
    lines = open(filename, 'r').read().split("\n")
    args = []
    atts = []
    for line in lines:
        if line.startswith("arg"):
            args.append(line.replace("arg(", "").replace(").", ""))
        elif line.startswith("att"):
            atts.append(line.replace("att(", "").replace(").", "").split(","))
    return args, atts

def print_usage():
    print("Usage:", os.path.basename(sys.argv[0]), "file mode var afmode sem q")
    print("Arguments:")
    print("    file : Input filename for AF instance in apx format.")
    print("    mode : Reasoning mode. mode={expl|diag}")
    print("        expl : explanation")
    print("        diag : diagnosis")
    print("    var  : Reasoning variant. var={arg|att}")
    print("        arg  : argument-based")
    print("        att  : attack-based")
    print("    afmode : Acceptance mode. acc={ca|cr|sa|sr}")
    print("        ca   : credulous acceptance")
    print("        cr   : credulous rejection")
    print("        sa   : skeptical acceptance")
    print("        sr   : skeptical rejection")
    print("    sem  : Argumentation semantics. sem={adm|com|stb}")
    print("        adm  : admissible")
    print("        com  : complete")
    print("        stb  : stable")
    print("    q    : Query argument.")

if __name__ == "__main__":
    if len(sys.argv) != 7:
        print_usage()
        sys.exit(1)
    if not os.path.isfile(".solverconfig"):
        print("Please run ./configure to set paths to Forqes, a MaxSAT solver, and QBF-SMUSer.")
        sys.exit(1)
    solver_config = open(".solverconfig", "r").read().split("\n")
    path_to_forqes = next(line for line in solver_config if line.startswith("FORQES_PATH")).replace("FORQES_PATH=","")
    path_to_maxsat = next(line for line in solver_config if line.startswith("MAXSAT_PATH")).replace("MAXSAT_PATH=","")
    path_to_qbfsmuser = next(line for line in solver_config if line.startswith("QBFSMUSER_PATH")).replace("QBFSMUSER_PATH=","")
    if "rc2" in path_to_maxsat:
        maxsat_solver = "rc2"
    _, filename, mode, var, afmode, semantics, query = sys.argv
    if mode not in ["expl", "diag"]:
        print("Reasoning mode", mode, "not supported.")
        sys.exit(1)
    if var not in ["arg", "att"]:
        print("Reasoning variant", var, "not supported.")
        sys.exit(1)
    att_mode = (var == "att")
    if afmode not in ["ca", "cr", "sa", "sr"]:
        print("Acceptance task", acc, "not supported.")
        sys.exit(1)
    skept_mode = (afmode[0] == "s")
    if semantics not in ["adm", "com", "stb"]:
        print("Semantics", semantics, "not supported.")
        sys.exit(1)
    args, atts = parse_af(filename)
    af_instance = AF(args, atts, filename, query, semantics, att_mode, skept_mode)
    if afmode == "cr" or afmode == "sa":
        if mode == "expl":
            smallest_explanation(af_instance)
        elif mode == "diag":
            smallest_diagnosis(af_instance)
    else:
        if mode == "expl":
            smallest_second_level(af_instance, False)
        elif mode == "diag":
            smallest_second_level(af_instance, True)