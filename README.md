selitae : Smallest Explanations and Diagnoses in Abstract Argumentation
=======================================================================

Version    : 2022.09.06

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki

References
----------

Please see the following publications for more information and details on `selitae`.

[Smallest Explanations and Diagnoses of Rejection in Abstract Argumentation.](https://proceedings.kr.org/2020/67/) Andreas Niskanen and Matti Järvisalo. In Proceedings of the 17th International Conference on Principles of Knowledge Representation and Reasoning (KR 2022).

[Computing Smallest MUSes of Quantified Boolean Formulas.](https://link.springer.com/chapter/10.1007/978-3-031-15707-3_23) Andreas Niskanen, Jere Mustonen, Jeremias Berg, and Matti Järvisalo. In Logic Programming and Nonmonotonic Reasoning (LPNMR 2022). Lecture Notes in Computer Science, volume 13416. Springer.

Installation
------------

In order to run `selitae`, please download [Forqes](https://alexeyignatiev.github.io/software/forqes/) and a MaxSAT solver of your choice.

**NEW:** Please also download and install [QBF-SMUSer](https://bitbucket.org/coreo-group/qbf-smuser) to make use of features explaining and diagnosing credulous acceptance and skeptical rejection.

Please run `./configure` before using this software to set the paths to the binary files of these solvers.

Usage
-----

```
Usage: selitae.py file mode var afmode sem q
Arguments:
    file : Input filename for AF instance in apx format.
    mode : Reasoning mode. mode={expl|diag}
        expl : explanation
        diag : diagnosis
    var  : Reasoning variant. var={arg|att}
        arg  : argument-based
        att  : attack-based
    afmode : Acceptance mode. acc={ca|cr|sa|sr}
        ca   : credulous acceptance
        cr   : credulous rejection
        sa   : skeptical acceptance
        sr   : skeptical rejection
    sem  : Argumentation semantics. sem={adm|com|stb}
        adm  : admissible
        com  : complete
        stb  : stable
    q    : Query argument.
```

The input file given via `file` must be an AF in apx format with the following predicates.

```
arg(x).   # x is an argument
att(x,y)  # (x,y) is an attack
```

Contact
-------

Please direct any questions, comments, or bug reports directly to [the current developer](mailto:andreas.niskanen@helsinki.fi).
